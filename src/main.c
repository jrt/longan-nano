#include "gd32v_pjt_include.h"
#include "lcd/lcd.h"
#include "console.h"
#include "init_nano.h"
#include <string.h>

int main(void)
{
    struct ConsoleBuffer buf;

    init_nano();

    // print the welcome message
    console_clear(&buf);
    console_write_string(&buf, "ChaOS v0.1.1\nOS is starting...\n");
    console_render(&buf);

    delay_1ms(1000);

    console_write_string(&buf, "$ _");
    console_render(&buf);

    while (1) {
        delay_1ms(500);
        
        if (buf.buf[buf.cursor-1] == ' ') {
            buf.buf[buf.cursor-1] = '_';
        } else {
            buf.buf[buf.cursor-1] = ' ';
        }
        console_render(&buf);
    }
}

int _put_char(int ch)
{
    usart_data_transmit(USART0, (uint8_t) ch );
    while ( usart_flag_get(USART0, USART_FLAG_TBE)== RESET){
    }

    return ch;
}
